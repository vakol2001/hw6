﻿using AutoMapper;
using ProjectManager.Application.LinqQueries.Models;
using ProjectManager.Application.Tasks.Commands;
using ProjectManager.Application.Tasks.Models;
using ProjectManager.Data.Entities;

namespace ProjectManager.Application.Infrastructure.Mapper.MappingProfiles
{
    public class TaskMappingProfile : Profile
    {
        public TaskMappingProfile()
        {
            CreateMap<Task, TaskModel>();
            CreateMap<Task, TaskNameModel>();
            CreateMap<CreateTaskCommand, Task>();
            CreateMap<UpdateTaskCommand, Task>();
        }
    }
}
