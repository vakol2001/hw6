﻿using ProjectManager.Application.Projects.Models;
using ProjectManager.Application.Tasks.Models;
using ProjectManager.Application.Users.Models;

namespace ProjectManager.Application.LinqQueries.Models
{
    public class UserInfoModel
    {
        public UserModel User { get; set; }
        public ProjectModel LastProject { get; set; }
        public int LastProjectAmountTasks { get; set; }
        public int AmountNotFinishedTasks { get; set; }
        public TaskModel LongestTask { get; set; }
    }
}
