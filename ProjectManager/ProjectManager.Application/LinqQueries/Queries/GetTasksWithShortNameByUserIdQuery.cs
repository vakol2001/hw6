﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ProjectManager.Application.Tasks.Models;
using ProjectManager.Data;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectManager.Application.LinqQueries.Queries
{
    public class GetTasksWithShortNameByUserIdQuery : IRequest<IEnumerable<TaskModel>>
    {
        public int Id { get; set; }

        public class Handler : IRequestHandler<GetTasksWithShortNameByUserIdQuery, IEnumerable<TaskModel>>
        {

            private readonly ProjectManagerContext _context;
            private readonly IMapper _mapper;

            public Handler(IMapper mapper, ProjectManagerContext context)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<IEnumerable<TaskModel>> Handle(GetTasksWithShortNameByUserIdQuery request, CancellationToken cancellationToken)
            {
                var tasks = await _context.Tasks
                                          .Where(t => t.PerformerId == request.Id)
                                          .Where(t => t.Name.Length < 45)
                                          .ToListAsync(cancellationToken);

                return _mapper.Map<IEnumerable<TaskModel>>(tasks);
            }
        }
    }
}
