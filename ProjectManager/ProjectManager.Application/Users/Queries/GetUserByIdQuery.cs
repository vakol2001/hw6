﻿using AutoMapper;
using MediatR;
using ProjectManager.Application.Users.Models;
using ProjectManager.Data;
using ProjectManager.Data.Entities;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectManager.Application.Users.Queries
{
    public class GetUserByIdQuery : IRequest<UserModel>
    {
        public int Id { get; set; }


        public class Handler : IRequestHandler<GetUserByIdQuery, UserModel>
        {
            private readonly ProjectManagerContext _context;
            private readonly IMapper _mapper;

            public Handler(IMapper mapper, ProjectManagerContext context)
            {
                _context = context;
                _mapper = mapper;
            }


            public async Task<UserModel> Handle(GetUserByIdQuery request, CancellationToken cancellationToken)
            {
                var user = await _context.Users.FindAsync(new object[] { request.Id }, cancellationToken)
                    ?? throw new KeyNotFoundException();

                var mappedUser = _mapper.Map<UserModel>(user);
                return mappedUser;
            }
        }
    }
}
