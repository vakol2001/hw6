﻿using MediatR;
using ProjectManager.Data;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectManager.Application.Teams.Commands
{
    public class DeleteTeamCommand : IRequest
    {
        public int Id { get; set; }

        public class Handler : IRequestHandler<DeleteTeamCommand>
        {
            private readonly ProjectManagerContext _context;

            public Handler(ProjectManagerContext context)
            {
                _context = context;
            }


            public async Task<Unit> Handle(DeleteTeamCommand request, CancellationToken cancellationToken)
            {
                var team = await _context.Teams.FindAsync(new object[] { request.Id }, cancellationToken) ?? throw new KeyNotFoundException();
                _context.Teams.Remove(team);
                await _context.SaveChangesAsync(cancellationToken);
                return Unit.Value;
            }

        }
    }
}
