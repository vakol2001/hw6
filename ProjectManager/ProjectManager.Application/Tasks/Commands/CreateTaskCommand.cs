﻿using AutoMapper;
using MediatR;
using ProjectManager.Application.Tasks.Models;
using ProjectManager.Data;
using System;
using System.Threading;
using System.Threading.Tasks;
using Task = ProjectManager.Data.Entities.Task;

namespace ProjectManager.Application.Tasks.Commands
{
    public class CreateTaskCommand : IRequest<TaskModel>
    {
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int State { get; set; }
        public DateTime? FinishedAt { get; set; }

        public class Handler : IRequestHandler<CreateTaskCommand, TaskModel>
        {
            private readonly ProjectManagerContext _context;
            private readonly IMapper _mapper;

            public Handler(IMapper mapper, ProjectManagerContext context)
            {
                _context = context;
                _mapper = mapper;
            }


            public async Task<TaskModel> Handle(CreateTaskCommand request, CancellationToken cancellationToken)
            {
                var taskToAdd = _mapper.Map<Task>(request);
                var task = await _context.Tasks.AddAsync(taskToAdd, cancellationToken);
                var mappedTask = _mapper.Map<TaskModel>(task.Entity);
                await _context.SaveChangesAsync(cancellationToken);
                return mappedTask;
            }
        }
    }
}
