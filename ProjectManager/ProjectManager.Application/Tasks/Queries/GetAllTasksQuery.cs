﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ProjectManager.Application.Tasks.Models;
using ProjectManager.Data;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectManager.Application.Tasks.Queries
{
    public class GetAllTasksQuery : IRequest<IEnumerable<TaskModel>>
    {
        public class Handler : IRequestHandler<GetAllTasksQuery, IEnumerable<TaskModel>>
        {
            private readonly ProjectManagerContext _context;
            private readonly IMapper _mapper;

            public Handler(IMapper mapper, ProjectManagerContext context)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<IEnumerable<TaskModel>> Handle(GetAllTasksQuery request, CancellationToken cancellationToken)
            {
                var tasks = await _context.Tasks.ToListAsync(cancellationToken);
                var mappedTasks = _mapper.Map<IEnumerable<TaskModel>>(tasks);
                return mappedTasks;
            }
        }
    }
}
